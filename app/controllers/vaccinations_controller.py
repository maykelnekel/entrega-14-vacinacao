from flask import request, jsonify, current_app
from app.models.vaccinations_model import VaccineCards
import sqlalchemy

def register_vaccination():
    try:
        data = request.get_json()
        for item in data:
            if type(data[item]) != str:
                return {"message": f"o campo {item} precisa ser uma string"}, 400
        if len(data['cpf']) != 11:
            return {"message": "o cpf deve ter 11 caracteres"}, 400

        filter_vaccination = current_app.db.session.query(VaccineCards).filter_by(cpf= data['cpf']).first()
        if filter_vaccination == None:
            vaccination = VaccineCards(**data)
            
            current_app.db.session.add(vaccination)
            current_app.db.session.commit()
            
            return jsonify(vaccination)
        else:
            return {"message": "cpf já cadastrado"}, 409
    except sqlalchemy.exc.IntegrityError as e:
        return {"message": "verifique se os dados estão corretos"}, 400

def get_vaccinations():
    vaccinations = VaccineCards.query.all()
    return jsonify(vaccinations)