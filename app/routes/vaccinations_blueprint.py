from flask import Blueprint, request, current_app
from flask.json import jsonify

from app.controllers.vaccinations_controller import register_vaccination, get_vaccinations


bp = Blueprint('vaccinations_bp', __name__, url_prefix='/vaccination')

bp.post("")(register_vaccination)
bp.get("")(get_vaccinations)