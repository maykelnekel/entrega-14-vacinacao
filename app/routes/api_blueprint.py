from flask import Blueprint
from app.routes import vaccinations_blueprint

bp = Blueprint('api_bp', __name__, url_prefix='/api')

bp.register_blueprint(vaccinations_blueprint.bp)

