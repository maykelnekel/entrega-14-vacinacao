from app.configs.database import db
from dataclasses import dataclass
from sqlalchemy import Column, String
from datetime import datetime, timedelta


@dataclass
class VaccineCards(db.Model):
    cpf: str
    name: str
    first_shot_date: str
    second_shot_date: str
    vaccine_name: str
    health_unit_name: str

    __tablename__ = 'vaccine_cards'

    first_shot = datetime.now()
    second_shot = first_shot + timedelta(days=90)
    

    cpf = Column(String(11), primary_key=True)
    name = Column(String, nullable=False)
    first_shot_date = first_shot
    second_shot_date = second_shot
    vaccine_name = Column(String, nullable=False)
    health_unit_name = Column(String, nullable=True)